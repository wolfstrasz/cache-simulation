#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>
#include <math.h>

typedef enum {dm, fa} cache_map_t;
typedef enum {fifo, lru, none} cache_replacement_t;
typedef enum {instruction, data} access_t;

typedef struct {
    uint32_t address;
    access_t accesstype;
} mem_access_t;

typedef struct {
    uint32_t instruction_accesses;
    uint32_t instruction_hits;
    uint32_t data_accesses;
    uint32_t data_hits;
} result_t;

/* Cache Parameters */
uint32_t cache_size = 2048; 
uint32_t block_size = 64;
cache_map_t cache_mapping = dm;
cache_replacement_t cache_replacement = none;

/*  Define new strsep that works on windows for testing. */
char* mystrsep(char** stringp, const char* delim)
{
  char* start = *stringp;
  char* p;

  p = (start != NULL) ? strpbrk(start, delim) : NULL;

  if (p == NULL)
  {
    *stringp = NULL;
  }
  else
  {
    *p = '\0';
    *stringp = p + 1;
  }

  return start;
}
/* End of definition of mystrsep */


/* Reads a memory access from the trace file and returns
 * 1) access type (instruction or data access)
 * 2) memory address
 */
mem_access_t read_transaction(FILE *ptr_file) {
    char buf[1002];
    char* token = NULL;
    char* string = buf;
    mem_access_t access;

    if (fgets(buf, 1000, ptr_file)!=NULL) {

        /* Get the access type */
        token = mystrsep(&string, " \n");
		   
        if (strcmp(token,"I") == 0) {
            access.accesstype = instruction;
        } else if (strcmp(token,"D") == 0) {
            access.accesstype = data;
        } else {
            printf("Unkown access type\n");
            exit(-1);
        }

        /* Get the address */        
        token = mystrsep(&string, " \n");
        access.address = (uint32_t)strtol(token, NULL, 16);

        return access;
    }

    /* If there are no more entries in the file return an address 0 */
    access.address = 0;
    return access;
}

void print_statistics(uint32_t num_blocks, uint32_t bits_offset, uint32_t bits_index, uint32_t bits_tag, result_t r) {
    /* Do Not Modify This Function */
    printf("Num_Blocks:%u\n", num_blocks);
    printf("Bits_BlockOffset:%u\n", bits_offset); 
    printf("Bits_Index:%u\n", bits_index); 
    printf("Bits_Tag:%u\n", bits_tag);
    if ( (r.instruction_accesses == 0) || (r.data_accesses == 0) ) {
        /* 
         * Just a protection over divide by zero.
         * Ideally, it should never reach here.
         */
        return;
    }
    printf("Total_Accesses:%u\n", r.instruction_accesses + r.data_accesses);
    printf("Total_Hits:%u\n", r.instruction_hits + r.data_hits);
    printf("Total_HitRate:%2.2f%%\n", (r.instruction_hits + r.data_hits) / ((float)(r.instruction_accesses + r.data_accesses)) * 100.0);
    printf("Instruction_Accesses:%u\n", r.instruction_accesses);
    printf("Instruction_Hits:%u\n", r.instruction_hits);
    printf("Instruction_HitRate:%2.2f%%\n", r.instruction_hits / ((float)(r.instruction_accesses)) * 100.0);
    printf("Data_Accesses:%u\n", r.data_accesses);
    printf("Data_Hits:%u\n", r.data_hits);
    printf("Data_HitRate:%2.2f%%\n", r.data_hits / ((float)(r.data_accesses)) * 100.0);
}

/*
 * Global variables
 * These variables must be populated before call to print_statistics(...) is made.
 */
 
uint32_t num_bits_for_block_offset = 0;
uint32_t num_bits_for_index = 0;
uint32_t num_bits_for_tag = 0;
uint32_t num_blocks = 0;
result_t result;

/* Add more global variables and/or functions as needed */
uint32_t curAddress = 0;		// stores the whole address of the current transaction
access_t curType;				// stores the type of the current transaction
uint32_t curIndex = 0; 			// stores the index 
uint32_t curTag = 0; 			// stores the tag of the address of the current transaction 
uint32_t curTagAndIndex = 0; 	// - stores the tag and index as integer
uint32_t * cacheBlocks; 		// - array of the blocks in the cache it keeps the tags
int hitFlag = 0;				// if hitFlag=1  if hit is found (used in FIFO mapping)

// for LRU mapping we will intialise a special kind of queue using 2 1-way graphs, variable for the first and last person.
//  - the lirst element will be the LRU element
//  - the last element will be the the MRU 
//		(the block we just wrote to or the block we found a hit)
//  - the queue is special because all of middle elements 
//		can go out of the queue and go back at the end to wait again
//		(when a hit is found in a middle element of the queue)
//  - as the largest cache is 8KB and the smallest block size is 16 the max num_blocks is 512
int curHit;						// - stores the number of the block if hit was found
int prevBlock [512];			// - a one way graph where prevBlock[i] gives the block that is less recently updated than the i-th
int nextBlock [512];			// - a one way graph where nextBlock[i] gives the block that is more recently updated than the i-th
int lastUsed = 0;				// - the value of lastUsed is the index of the block in the end of the queue e.g. the MRU element
int toBeUpdated = 0;			// - the value is the index of the block which is to be evicted e.g the LRU element


/* Set the cache info */
void setCacheInfo()
{
	num_blocks = cache_size / block_size;										// set number of blocks in cache
	cacheBlocks = (uint32_t *) calloc (num_blocks, sizeof(uint32_t));			// malloc a cache of size num_blocks
	num_bits_for_block_offset  = log2(block_size);								// set number of bits for block offset
	if(cache_mapping == dm)
		num_bits_for_index = log2(num_blocks);									// if DM set the number of bits for the index
	if(cache_mapping == fa)
		num_bits_for_index = 0;													// if FA no bits for index
	num_bits_for_tag = 32 - (num_bits_for_index + num_bits_for_block_offset);	// set number of bits for the tag
	
	
	 /*print for debugging
	printf("Number of blocks: %u \n",num_blocks);					// DEBUG:START
	printf("Bits for offset:  %u \n",num_bits_for_block_offset);	// *
	printf("Bits for index:   %u \n",num_bits_for_index);			// *
	printf("Bits for tag:     %u \n",num_bits_for_tag);				// DEBUG:END
	*/
	
	// if we have LRU FA mapping then we need to create our queue.
	if(cache_mapping == fa && cache_replacement == lru)	
		createLRUqueue();
	 	
}									

/* For debugging purposes it prints out the queue graph for the LRU mapping */
void printGraph()
{
	int i=toBeUpdated;
	int j=0;
	//printf("         ");
	for(j=0;j<num_blocks;j++)
	{
		printf("(%d) --> ", i );
		i = nextBlock[i];
	}
	printf( "(%d)  end \n",i);
	
	i = lastUsed;
	for(j=0;j<num_blocks;j++)
	{
		printf("(%d) <-- ",i);
		i = prevBlock[i];
	}
	printf( "(%d) end \n", i);
}
/* End of printGraph() */


/* creates the start queue for the LRU mapping */
void createLRUqueue()
{
	int i=0;
	toBeUpdated = 0;
	lastUsed = num_blocks - 1;
	
	nextBlock [0] = 1;
	prevBlock [0] = num_blocks -1;
	//printf("prev block %d \n",prevBlock[0]);
	for(i=1;i<num_blocks-1;i++)
	{
		nextBlock [i] = i+1;
		prevBlock [i] = i-1;		
	}
	
	nextBlock[lastUsed] = 0;
	prevBlock[lastUsed] = lastUsed - 1;
	//printGraph();
}
/* End of createLRUqueue(); */


/* If blocks in cache is only 1 then there are no mapping methods used
	as they will check the only element and have a hit or evict it and put the new one. */
void noMapping()
{
	if(curTag == cacheBlocks[0])
	{
		//printf("HIT FOUND!!! \n");
		if(curType == data) result.data_hits++;
		if(curType == instruction) result.instruction_hits++;
		
	}
	else
	{
		cacheBlocks[0] = curTag;
		//printf("MISS FOUND!!! \n");
	}
}
/* If the mapping is direct */
void directMapping()
{
//	printf("**ENTERING directMapping: \n");
	if(curTag == cacheBlocks[curIndex])
	{
//		printf("HIT FOUND!!! \n");
		if(curType == data)result.data_hits++;
		if(curType == instruction)result.instruction_hits++;		
	}
	else
	{
//		printf("MISS FOUND!!! \n");
		cacheBlocks[curIndex] = curTag;
	}
}

/* If mapping is associative and FIFO replacement is used */
void fifoFAMapping()
{
	//	printf("**ENTERING fifoFAMapping : \n");
	hitFlag=0;
	
	int i = 0;
	//printf("cacheBlocks[%u] = %u ! \n",i,cacheBlocks[i]);
	/* searching until we reach the end of the cache or an empty block or a hit. */
	while( i<num_blocks && cacheBlocks[i] != 0 && hitFlag == 0) 
	{
		
	//printf("cacheBlocks[%u] = %u ! \n",i,cacheBlocks[i]);
		/* if it finds a hit it marks it and sets the flag to 1; */
		if(curTag == cacheBlocks[i]) 
		{
			// printf("HIT FOUND!!! \n");
			if(curType == data)result.data_hits++;
			if(curType == instruction)result.instruction_hits++;
			hitFlag = 1;
		}
		i++;
	}
	
	if(hitFlag == 0) // if no hit was found fetch.
	{
		// printf("MISS FOUND!!! \n");
		cacheBlocks[toBeUpdated] = curTag;
		toBeUpdated ++;
		/* if the reaches the end of the cache blocks start from beginning (FIFO replacement). */
		if(toBeUpdated >= num_blocks)toBeUpdated = 0; 
	}
}

/* If mapping is associative and LRU replacement is used */
void lruFAMapping()
{
	
	//printf("*** LRU MAPPING: \n");
	//printGraph();
	curHit = -1; // we cannot use 0 as it is the number of the first block.
	int i;
	for (i = 0; i<num_blocks; i++) // pass over all blocks
	{
		
	//  printf("PASS: %d\n", i);
		if(curTag == cacheBlocks[i])
		{
			curHit = i;
		}
	}
	// printf("HIT FLAG AFTER FOR IS : %d \n", curHit);
	if(curHit != -1 )
	{
	//	printf("HIT FOUND!!! \n");
		if(curType == data)result.data_hits++;
		if(curType == instruction)result.instruction_hits++;
		
		if(curHit == toBeUpdated)
		{
			lastUsed = toBeUpdated;
			toBeUpdated = nextBlock[toBeUpdated];
		}
		else if(curHit == lastUsed)	
		{
			/* you do nothing as the MRU is again the MRU */
		}
		else /* if hit on a block that is neither LRU nor MRU */
		{
			nextBlock[prevBlock[curHit]] = nextBlock[curHit];
			prevBlock[nextBlock[curHit]] = prevBlock[curHit];
			
			/* transport it to the end and connect */
			nextBlock[curHit] = toBeUpdated;
			prevBlock[toBeUpdated] = curHit;
			
			/* connect it to the last used */
			prevBlock[curHit] = lastUsed;											
			nextBlock[lastUsed] = curHit;
			
			/* set the hit block as last used*/
			lastUsed = curHit;
			
		}
	}
	
	if (curHit == -1)
	{
		//printf("MISS FOUND!!! \n");
		cacheBlocks[toBeUpdated] = curTag;
		lastUsed = toBeUpdated;
		toBeUpdated = nextBlock[toBeUpdated];
	}
	
}



int main(int argc, char** argv)
{

    /*
     * Read command-line parameters and initialize:
     * cache_size, block_size, cache_mapping and cache_replacement variables
     */

    if ( argc != 4 ) { /* argc should be 4 for correct execution */
        printf("Usage: ./cache_sim [cache size: 64-8192] [cache block size: 32/64/128] [cache mapping: DM/FIFO/LRU]\n");
        exit(-1);
    } else  {
        /* argv[0] is program name, parameters start with argv[1] */

        /* Set block and cache size in bytes */
        cache_size = atoi(argv[1]);
        block_size = atoi(argv[2]);
        assert(cache_size >= 256 && cache_size <= 8192);
        /* cache_size must be power of 2 */
        assert(!(cache_size & (cache_size-1)));
        assert(block_size >= 16 && block_size <= 256);
        /* block_size must be power of 2 */
        assert(!(block_size & (block_size-1)));
        assert(block_size <= cache_size);

        /* Set Cache Mapping */
        if (strcmp(argv[3], "DM") == 0) {
            cache_mapping = dm;
            cache_replacement = none;
        } else if (strcmp(argv[3], "FIFO") == 0) {
            cache_mapping = fa;
            cache_replacement = fifo;
        } else if (strcmp(argv[3], "LRU") == 0 ) {
            cache_mapping = fa;
            cache_replacement = lru;
        } else {
            printf("Unknown cache mapping: %s\n", argv[3]);
            exit(-1);
        }
        

    } 


    /* Open the file mem_trace.txt to read memory accesses */
    FILE *ptr_file;
    ptr_file =fopen("mem_trace.txt","r");
    if (!ptr_file) {
        printf("Unable to open the trace file\n");
        exit(-1);
    }

    /* Reset the result structure */
    memset(&result, 0, sizeof(result));

    /* Set CacheInfo() */
    setCacheInfo();
    
    /* Set the method to be used in when accessing the cache */
    void (*seek)(void); 																// Create function pointer
    if(num_blocks == 1) 										seek = &noMapping;		// No mapping used (only 1 block)
    else if(cache_mapping == fa && cache_replacement == fifo) 	seek = &fifoFAMapping;	// FIFO Full Associative Mapping used
	else if(cache_mapping == fa && cache_replacement == lru)   	seek = &lruFAMapping;	// LRU Full Associative Mapping used
	else if(cache_mapping == dm )								seek = &directMapping;	// Direct Mapping used

    /* Loop until whole trace file has been read */
    mem_access_t access;
    while(1) {
    	
        access = read_transaction(ptr_file);
        /* If no transactions left, break out of loop */
        if (access.address == 0)
           break;
        
        /* Increment the accesses  */
        if(access.accesstype == data)result.data_accesses++;
        if(access.accesstype == instruction)result.instruction_accesses++;
        
        /* split access info to parts (curAddress, curType, curTagAndIndex, curIndex, curTag). */
		curAddress = access.address;								//set curAdress
		curType = access.accesstype;								//set curType
		curTagAndIndex = curAddress>> num_bits_for_block_offset;	//remove the bits for the offset
		if(num_bits_for_index > 0)
			curIndex = curTagAndIndex % num_blocks;					// num_blocks = 2^(num_bits_for_index) 
		curTag = curTagAndIndex >> num_bits_for_index;				// set curTag to all bits left.
	
		/* Print debugging for each transaction: 			//DEBUG: START
		printf(" ----------- NEW ACCESS : \n");				// *
		if(curType == instruction)							// *
		printf("Access type:      Instruction \n");			// *
		if(curType == data)									// *
		printf("Access type:      Data \n");				// *
		printf("Address full:     %u \n", curAddress);		// *
		printf("Address noOffset: %u \n", curTagAndIndex);	// *
		printf("Address index:    %u \n", curIndex);		// *
		printf("Address tag:      %u \n", curTag);			//DEBUG:END
		*/
		
		seek();
		
    }
    
    print_statistics(num_blocks, num_bits_for_block_offset, num_bits_for_index, num_bits_for_tag, result);
    
    /* Close the trace file */
    fclose(ptr_file);

    return 0;
}
