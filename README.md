# Cache simulator

The project simulatates the process of getting data from a cache. It receives as input a stream of access calls to addresses, simulates what happens inside the cache and analyses how many hits and misses it has found, returning the analysis.
There are two types of cache associativities handled by the project:
* Direct Mapping  (DM)
* Fully Associative (LRU / FIFO)

The Fully Associative cache has two different re-placement policies:
* LRU - least-recently used
* FIFO - first in first out


### Prerequisites

Download the git repository.

### Installing

Compile cache_sim.c with any C compiler.

e.g. GCC:

```
gcc -cache_sim.c -o cache_sim
```

## Example test
Test are simple txt file in format:
* File = mem_trace.txt
* It should be separated in small strings represented as "C XXXXXXXX" (Access Address) separated by newline
* C - character representing type of access (I/D) I - instruction / D - data
* X - character representing hexadecimal digit (1-9,a,b,c,d,e,f)
* XXXXXXXX - represents a 8-digit hexadecimal number

Example test:
```
I 12d202d0
D ab76a48
I 12d202d3
D ab76a40
I 12d23a70
I 12d23a71
D ab76a38
```

## Testing
Everytime the program is ran it requires three arguments
* Cache size = [64-8192] (mainly)
* Block size = 32 / 64 / 128 (mainly)
* Cache mapping = DM / FIFO / LRU 

```
./cache_sim 64 32 DM
```

## Authors

* **Boyan Yotov** - *Full work* - [wolfstrasz](https://gitlab.com/wolfstrasz)


## Acknowledgments

* Billie Thompson for creating the template. (https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#file-readme-template-md)

